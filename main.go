package main

import (
	"log"
	"net/http"
	"os"
	"quickstart/cache"
	"quickstart/config"
	locationhttp "quickstart/internal/location/delivery/http"
	locationrepository "quickstart/internal/location/repository"
	locationusercase "quickstart/internal/location/usecase"
	"quickstart/internal/middleware"

	placehttp "quickstart/internal/place/delivery/http"
	placerepository "quickstart/internal/place/repository"
	placeusecase "quickstart/internal/place/usecase"

	userhttp "quickstart/internal/user/delivery/http"
	userrepository "quickstart/internal/user/repository"
	userusecase "quickstart/internal/user/usecase"

	uploadhttp "quickstart/internal/upload/delivery/http"

	"quickstart/pkg/db/mysql"
	"quickstart/pkg/db/redis"
	"quickstart/pkg/upload"
	"quickstart/pkg/utils"
	"strings"

	"github.com/gin-gonic/gin"
)

func main() {
	env := os.Getenv("ENV")
	fileName := "./config/config-local.yml"
	if strings.ToLower(env) == "prod" {
		fileName = "./config/config-prod.yml"
	}
	cfg, err := config.LoadConfig(fileName)
	if err != nil {
		log.Fatalln("db connection err: ", err)
	}

	db, err := mysql.MySQLConnection(cfg)
	if err != nil {
		log.Fatal("Cannot connect mysql: ", err)
		return
	}
	// db.AutoMigrate(&placemodel.Place{})
	// utils.RunDBMigration(cfg)

	/*-----------khai báo redis-----*/
	redis, err := redis.NewRedisClient(cfg)
	if err != nil {
		log.Fatalln("Cannot connect redis: ", err)
	}
	//Khai báo s3
	s3Provider := upload.NewS3Provider(cfg)
	//Khai báo logger
	// sugarLogger := logger.NewZapLogger()

	hasher := utils.NewHashIds(cfg.App.Secret, 10)

	/*------------khai báo các dependency--------------*/
	//dependency of location module
	locationRepo := locationrepository.NewLocationRepository(db)
	locationUC := locationusercase.NewLocationUsecase(locationRepo)
	locationHandler := locationhttp.NewLocationHandler(locationUC)

	//dependency of place module
	placeRepo := placerepository.NewPlaceRepository(db)
	placeUC := placeusecase.NewPlaceUseCase(placeRepo)
	placeHandler := placehttp.NewPlaceHandler(placeUC, hasher)

	//dependency of use module
	userRepo := userrepository.NewUserRepository(db)
	authUserCache := cache.NewAuthUserCache(userRepo, cache.NewRedisCache(redis), hasher)
	userUC := userusecase.NewUserUseCase(userRepo, cfg)
	userHandler := userhttp.NewUserHandler(userUC, hasher)

	//dependency of upload module
	uploadHandler := uploadhttp.NewUploadHandler(s3Provider)
	////dependency of middle module
	// middleware := middleware.NewMiddlewareManager(cfg, userRepo)
	middleware := middleware.NewMiddlewareManager(cfg, authUserCache)

	router := gin.Default()
	router.Use(middleware.Recover())
	r := router.Group("/api/v1")
	r.Static("/static", "./static")

	r.POST("/upload", uploadHandler.Upload())
	r.GET("/profiles", middleware.RequireAuth(), func(c *gin.Context) {
		user := c.MustGet("user")
		c.JSON(http.StatusOK, gin.H{"data": user})
	})
	r.GET("/admin", middleware.RequireAuth(), middleware.RequiredRoles("guest"), func(ctx *gin.Context) { ctx.JSON(http.StatusOK, gin.H{"data": "Ok"}) })

	r.GET("/location/:id", locationHandler.FindLocationById())
	r.GET("/locations")

	r.POST("/places", middleware.RequireAuth(), middleware.RequiredRoles("host", "admin"), placeHandler.CreatePlace())
	r.GET("/places", placeHandler.GetPlaces())
	r.GET("/place/:id", placeHandler.GetPlaceByID())
	r.PUT("/place/:id", middleware.RequireAuth(), middleware.RequiredRoles("host", "admin"), placeHandler.UpdatePlace())
	r.POST("/place/:id", middleware.RequireAuth(), middleware.RequiredRoles("host", "admin"), placeHandler.UpdatePlace())
	r.DELETE("/place/:id", middleware.RequireAuth(), middleware.RequiredRoles("host", "admin"), placeHandler.DeletePlace())

	r.POST("/user/register", userHandler.Register())
	r.POST("user/login", userHandler.Login())
	r.DELETE("user/:id", middleware.RequireAuth(), userHandler.DeleteUserById())

	router.Run(":" + cfg.App.Port)
}
