package common

type appResponse struct {
	Data   interface{} `json:"data"`
	Paging interface{} `json:"paging,omitempty"`
}

func NewSuccessResponse(data interface{}, paging interface{}) *appResponse {
	return &appResponse{Data: data, Paging: paging}
}

func Response(data interface{}) *appResponse {
	return NewSuccessResponse(data, nil)
}

func ResponseWithPaging(data interface{}, paging interface{}) *appResponse {
	return NewSuccessResponse(data, paging)
}
