package common

import (
	"fmt"
	"net/http"
)

type AppError struct {
	Causes     error  `json:"-"` // lỗi gốc của chương trình
	StatusCode int    `json:"status"`
	Message    string `json:"message"`
	//Dung làm đa ngôn ngữ
	// Key        string `json:"key"`
}

func NewErrorResponse(causes error, status int, msg string) *AppError {
	return &AppError{Causes: causes, StatusCode: status, Message: msg}
}

func (e *AppError) RootCauses() error {
	if err, ok := e.Causes.(*AppError); ok {
		return err.RootCauses()
	}
	return e.Causes
}

func (e *AppError) Error() string {
	return e.RootCauses().Error()
}

func ErrUnauthorized(cause error) *AppError {
	return NewErrorResponse(cause, http.StatusUnauthorized, "you have no authorization")
}

func ErrForbidden(cause error) *AppError {
	return NewErrorResponse(cause, http.StatusForbidden, "you have no permission")
}

func ErrBadRequest(cause error) *AppError {
	return NewErrorResponse(cause, http.StatusBadRequest, "invalid request")
}

func ErrNotFound(cause error) *AppError {
	return &AppError{Causes: cause, StatusCode: http.StatusNotFound, Message: "not found"}
}

func ErrDB(cause error) *AppError {
	return &AppError{Causes: cause, StatusCode: http.StatusInternalServerError, Message: "something went wrong with Database"}
}

func ErrInternal(cause error) *AppError {
	return &AppError{cause, http.StatusInternalServerError, "something went wrong in the server"}
}

func NewCustomErr(cause error, msg string) *AppError {
	return NewErrorResponse(cause, http.StatusBadRequest, msg)
}

func ErrCannotListEntity(entity string, cause error) *AppError {
	return NewCustomErr(cause, fmt.Sprintf("can't list %s", entity))
}

func ErrEntityNotFound(entity string, cause error) *AppError {
	return NewCustomErr(cause, fmt.Sprintf("can't find %s", entity))
}

func ErrCannotGetEntity(entity string, cause error) *AppError {
	return NewCustomErr(cause, fmt.Sprintf("can't get %s", entity))
}

func ErrCannotCreateEntity(entity string, cause error) *AppError {
	return NewCustomErr(cause, fmt.Sprintf("can't create %s", entity))
}

func ErrCannotUpdateEntity(entity string, cause error) *AppError {
	return NewCustomErr(cause, fmt.Sprintf("can't update %s", entity))
}

func ErrCannotDeleteEntity(entity string, cause error) *AppError {
	return NewCustomErr(cause, fmt.Sprintf("can't delete %s", entity))
}

func ErrEntityExisted(entity string, cause error) *AppError {
	return NewCustomErr(cause, fmt.Sprintf("%s already existed", entity))
}
