package mysql

import (
	"fmt"
	"log"
	"quickstart/config"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func MySQLConnection(config *config.Config) (*gorm.DB, error) {
	cfg := config.MySQL
	user := cfg.User
	password := cfg.Password
	host := cfg.Host
	publicPort := cfg.Port
	DBName := cfg.DBName

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", user, password, host, publicPort, DBName)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Failed connect to MySQL")
		return nil, err

	}
	fmt.Println("Connect to MySQL successfully", db)
	return db, nil
}
