package main

import (
	"strconv"

	"go.uber.org/zap"
)

func main() {
	//khởi tạo zap
	logger, _ := zap.NewProduction()
	sugar := logger.Sugar()
	//xả buffer trước khi exit
	defer logger.Sync()
	_, err := strconv.Atoi("asd")
	logger.Error("Error convert string to int", zap.Error(err))
	sugar.Errorf("Error convert string %s to int, Error is %s", "sad", err)
}
