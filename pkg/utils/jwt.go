package utils

import (
	"errors"
	"fmt"
	"quickstart/config"
	"quickstart/pkg/common"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type Token struct {
	AccessToken string `json:"accessToken"`
	ExpireAt    int64  `json:"expireAt"`
}

type TokenPayload struct {
	Email string `json:"email"`
	Role  string `json:"role"`
}

type claim struct {
	Playload TokenPayload `json:"payload"`
	jwt.RegisteredClaims
}

func GenerateJWT(data TokenPayload, cfg *config.Config) (*Token, error) {

	expirationTime := jwt.NewNumericDate(time.Now().Add(time.Hour * 12))
	claim := claim{
		Playload: data,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: expirationTime,
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			ID:        fmt.Sprintf("%d", jwt.NewNumericDate(time.Now()).UnixNano()),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	accessToken, err := token.SignedString([]byte(cfg.App.Secret))
	if err != nil {
		return nil, err
	}

	return &Token{
		AccessToken: accessToken,
		ExpireAt:    expirationTime.Unix(),
	}, nil
}

func ValidateJWT(accessToken string, cfg *config.Config) (*TokenPayload, error) {

	token, err := jwt.ParseWithClaims(accessToken, &claim{}, func(t *jwt.Token) (interface{}, error) {
		return []byte(cfg.App.Secret), nil
	})

	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, errors.New("invalid token")
	}

	claim, ok := token.Claims.(*claim)
	if !ok {
		return nil, errors.New("invalid token")
	}
	return &claim.Playload, nil
}

func RefreshJWT(accessToken string, cfg *config.Config) (*Token, error) {
	token, err := jwt.ParseWithClaims(accessToken, &claim{}, func(t *jwt.Token) (interface{}, error) {
		return []byte(cfg.App.Secret), nil
	})

	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, ErrInvalidToken
	}

	claim, ok := token.Claims.(*claim)

	if !ok {
		return nil, ErrInvalidToken
	}

	expirationTime := jwt.NewNumericDate(time.Now().Add(12 * time.Hour))
	claim.ExpiresAt = expirationTime
	claim.IssuedAt = jwt.NewNumericDate(time.Now())
	claim.ID = fmt.Sprintf("%d", time.Now().UnixNano())

	token = jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	accessToken, err = token.SignedString([]byte(cfg.App.Secret))
	if err != nil {
		return nil, err
	}

	return &Token{
		AccessToken: accessToken,
		ExpireAt:    expirationTime.Unix(),
	}, nil
}

var (
	ErrTokenNotFound = common.ErrUnauthorized(errors.New("token not found"))

	ErrEncodingToken = common.ErrUnauthorized(errors.New("error encoding token"))

	ErrInvalidToken = common.ErrUnauthorized(errors.New("invalid token"))
)
