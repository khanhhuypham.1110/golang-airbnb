.PHONY: create_mysql migrate_up migrate_down

# Go migrate
create_mysql:
	docker run --name go01-mysql -e MYSQL_ROOT_PASSWORD=1234 -p 3307:3306 -d mysql:latest

create_redis: 
	docker run --name go01-redis -e ALLOW_EMPTY_PASSWORD=yes -p 6379:6379 -d redis:latest

# Go migrate
migrate_up:
	migrate -path migrations -database "mysql://root:1234@tcp(127.0.0.1:3307)/go01-airbnb" up

migrate_down:
	migrate -path migrations -database "mysql://root:1234@tcp(127.0.0.1:3307)/go01-airbnb" down


# Main
run:
	go run main.go 1

build:
	go build main.go