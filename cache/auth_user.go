package cache

import (
	"context"
	"fmt"
	usermodel "quickstart/internal/user/model"
	"quickstart/pkg/common"
	"quickstart/pkg/utils"
	"time"
)

const cacheKey = "user:%d"

type UserStore interface {
	FindDataWithCondition(ctx context.Context, condition map[string]any) (*usermodel.User, error)
}

type authUserCache struct {
	store      UserStore //mysql
	cacheStore Cache     //redis
	hasher     *utils.Hasher
}

func NewAuthUserCache(store UserStore, cacheStore Cache, hasher *utils.Hasher) *authUserCache {
	return &authUserCache{
		store:      store,
		cacheStore: cacheStore,
		hasher:     hasher,
	}
}

func (authUserCache *authUserCache) FindDataWithCondition(ctx context.Context, condition map[string]any) (*usermodel.User, error) {
	var user usermodel.User
	userEmail := condition["email"]
	key := fmt.Sprintf(cacheKey, userEmail) //key lưu xuống redis
	//Cố gắng tìm data của user từ cache trước
	authUserCache.cacheStore.Get(ctx, key, &user)
	//tìm được data trong cache return về data đó
	if user.Id > 0 && userEmail != "" {
		return &user, nil
	}

	//Tìm data user ở dưới DB
	u, err := authUserCache.store.FindDataWithCondition(ctx, condition)
	if err != nil {
		return nil, err
	}
	//lưu data user xuống cache
	u.FakeId = authUserCache.hasher.Encode(u.Id, common.DBTypeUser)
	authUserCache.cacheStore.Set(ctx, key, u, time.Hour*2)
	return u, nil
}
