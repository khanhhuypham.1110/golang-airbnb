package placeamenitymodel

import (
	"time"

	"gorm.io/gorm"
)

type PlaceAmenityModel struct {
	AmenitiesId     int       `json:"-" gorm:"column:amenities_id"`
	AmenitiesFakeId string    `json:"amenitiesId" gorm:"-"`
	PlaceId         int       `json:"-" gorm:"column:place_id"`
	PlaceFakeId     string    `json:"placeId" gorm:"-"`
	CreatedAt       time.Time `json:"createdAt" gorm:"column:created_at"`
	UpdatedAt       time.Time `json:"updatedAt" gorm:"column:updated_at"`
	//gorm.DeleteAt: use to perform soft deletion
	DeletedAt gorm.DeletedAt `json:"-" gorm:"column:deleted_at"`
}
