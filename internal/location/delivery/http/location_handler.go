package locationhttp

import (
	"context"
	"net/http"
	locationmodel "quickstart/internal/location/model"
	"quickstart/pkg/common"
	"strconv"

	"github.com/gin-gonic/gin"
)

type LocationUsecase interface {
	FindLocationById(ctx context.Context, id int) (*locationmodel.Location, error)
	// FindLocationByKeyWord(ctx context.Context, keyword int) ([]locationmodel.Location, error)
	// ListAllLocation(ctx context.Context, paging *common.Paging) ([]locationmodel.Location, error)
}
type locationHandler struct {
	locationUC LocationUsecase
}

func NewLocationHandler(locationUC LocationUsecase) *locationHandler {
	return &locationHandler{locationUC: locationUC}
}

func (handler locationHandler) FindLocationById() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			panic(common.ErrBadRequest(err))
		}
		location, err := handler.locationUC.FindLocationById(ctx, id)
		if err != nil {
			panic(err)
		}
		location.FakeId = ctx.Param("id")
		ctx.JSON(http.StatusOK, common.Response(location))
	}
}
