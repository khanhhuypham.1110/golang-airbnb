package locationusercase

import (
	"context"
	locationmodel "quickstart/internal/location/model"
	"quickstart/pkg/common"
)

type LocationRepository interface {
	ListLocationWithCondition(ctx context.Context, paging *common.Paging, keys ...string) ([]locationmodel.Location, error)
	FindLocationWithCondition(ctx context.Context, condition map[string]any, keys ...string) (*locationmodel.Location, error)
}

type locationUsecase struct {
	locationRepo LocationRepository
}

func NewLocationUsecase(repo LocationRepository) *locationUsecase {
	return &locationUsecase{locationRepo: repo}
}

func (locationUC locationUsecase) FindLocationById(ctx context.Context, id int) (*locationmodel.Location, error) {

	location, err := locationUC.locationRepo.FindLocationWithCondition(ctx, map[string]any{"id": id})
	if err != nil {
		return nil, common.ErrEntityNotFound(locationmodel.EntityName, err)
	}
	return location, nil
}

// func (locationUc locationUsecase) FindLocationByKeyWord(ctx context.Context, keyword string) ([])
