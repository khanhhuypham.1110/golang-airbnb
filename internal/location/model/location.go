package locationmodel

import (
	"errors"
	"quickstart/pkg/common"
)

const EntityName = "location"

type Location struct {
	common.SQLModel
	Country  string `json:"country" gorm:"column:country"`
	State    string `json:"state" gorm:"column:state"`
	Province string `json:"province" gorm:"column:province"`
	City     string `json:"city" gorm:"column:city"`
	District string `json:"district" gorm:"column:district"`
	Ward     string `json:"ward" gorm:"column:ward"`
}

func (Location) TableName() string {
	return "locations"
}

func (location Location) Validate() error {
	if location.Country != "" {
		return ErrCountryIsEmpty
	}
	return nil
}

var (
	ErrCountryIsEmpty = common.NewCustomErr(errors.New("country can't be blank"), "country can't be blank")
	ErrCityIsEmpty    = common.NewCustomErr(errors.New("city can't be blank"), "citycan't be blank")
)
