package locationrepository

import (
	"context"
	locationmodel "quickstart/internal/location/model"
	"quickstart/pkg/common"

	"gorm.io/gorm"
)

type locationRepository struct {
	db *gorm.DB
}

func NewLocationRepository(db *gorm.DB) *locationRepository {
	return &locationRepository{db: db}
}

func (repo *locationRepository) ListLocationWithCondition(
	ctx context.Context,
	paging *common.Paging,
	keys ...string) ([]locationmodel.Location, error) {
	var locationList []locationmodel.Location
	db := repo.db.Table(locationmodel.Location{}.TableName())

	if err := db.Count(&paging.Total).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	for _, v := range keys {
		repo.db.Preload(v)
	}

	if v := paging.Cursor; v > 0 {
		db.Where("id > ?", v)
	} else {
		offset := (paging.Page - 1) * paging.Limit
		if err := db.Offset(offset).Error; err != nil {
			return nil, common.ErrDB(err)
		}
	}

	if err := db.Limit(paging.Limit).Find(&locationList).Error; err != nil {
		return nil, common.ErrDB(err)
	}
	if len(locationList) > 0 {
		paging.NextCursor = locationList[len(locationList)-1].Id
	}

	return locationList, nil
}

func (repo *locationRepository) FindLocationWithCondition(ctx context.Context, condition map[string]any, keys ...string) (*locationmodel.Location, error) {
	var location locationmodel.Location
	db := repo.db.Table(locationmodel.Location{}.TableName())

	for _, v := range keys {
		db.Preload(v)
	}

	if err := db.Where(condition).First(&location).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.ErrEntityNotFound(locationmodel.EntityName, err)
		}
		return nil, common.ErrDB(err)
	}

	return &location, nil
}
