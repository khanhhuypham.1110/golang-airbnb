package uploadmodel

import "quickstart/pkg/common"

const EntityName = "Upload"

type Upload struct {
	common.Image
}
