package uploadhttp

import (
	"bytes"
	"fmt"
	"image"
	"quickstart/pkg/common"
	"quickstart/pkg/upload"

	"github.com/gin-gonic/gin"
)

type uploadHandler struct {
	s3Provider upload.UploadProvider
}

func NewUploadHandler(s3Provider upload.UploadProvider) *uploadHandler {
	return &uploadHandler{s3Provider: s3Provider}
}

func (handler *uploadHandler) Upload() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fileHeader, err := ctx.FormFile("file")
		if err != nil {
			panic(common.ErrBadRequest(err))
		}
		// //save file trực tiếp lên server của mình
		// if err := ctx.SaveUploadedFile(fileHeader, fmt.Sprintf("static/%s", fileHeader.Filename)); err != nil {
		// 	panic(err)
		// }
		// ctx.JSON(http.StatusOK, common.Response(common.Image{
		// 	Url: "http://localhost:4000/static/" + fileHeader.Filename,
		// }))

		folder := ctx.DefaultPostForm("folder", "img")
		fileName := fileHeader.Filename //happ.png
		file, err := fileHeader.Open()
		if err != nil {
			panic(common.ErrBadRequest(err))
		}
		defer file.Close()
		dataBytes := make([]byte, fileHeader.Size)
		if _, err := file.Read(dataBytes); err != nil {
			panic(common.ErrBadRequest(err))
		}

		//Lấy width, height của image
		// w, h, err := getImageDimension(fileBytes)
		// if err != nil {
		// 	//File không phải là hình ảnh
		// 	panic(common.ErrBadRequest(err))
		// }
		img, err := handler.s3Provider.UploadFile(ctx.Request.Context(), dataBytes, fmt.Sprintf("%s/%s", folder, fileName))

		if err != nil {
			//Không thể upload file
			panic(common.ErrBadRequest(err))
		}

		// img.Height = h
		// img.Width = w
		ctx.JSON(200, common.Response(img))

	}
}

func getImageDimension(dataBytes []byte) (int, int, error) {
	fileBytes := bytes.NewBuffer(dataBytes)
	img, _, err := image.DecodeConfig(fileBytes)
	if err != nil {
		return 0, 0, err
	}
	return img.Width, img.Height, nil
}
