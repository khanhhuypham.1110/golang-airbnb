package amenitymodel

import (
	"quickstart/pkg/common"
)

const EntityName = "amenity"

type Amenity struct {
	common.SQLModel
	Name        string         `json:"name" gorm:"column:name"`
	Description string         `json:"description" gorm:"column:description"`
	Icon        *common.Images `json:"icon" gorm:"column:icon"`
}

func (Amenity) TableName() string {
	return "Amenities"
}
