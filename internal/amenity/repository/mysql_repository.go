package amenityrepository

import (
	"context"
	amenitymodel "quickstart/internal/amenity/model"
	"quickstart/pkg/common"

	"gorm.io/gorm"
)

type amenityrepository struct {
	db *gorm.DB
}

func NewAmenityRepository(db *gorm.DB) *amenityrepository {
	return &amenityrepository{db: db}
}

func (repo *amenityrepository) ListAllAmenitiesWithCondition(ctx context.Context, paging *common.Paging) ([]amenitymodel.Amenity, error) {
	var amenities []amenitymodel.Amenity
	db := repo.db.Table(amenitymodel.Amenity{}.TableName())
	db = db.Model(&amenities)

	if err := db.Count(&paging.Total).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	if v := paging.Cursor; v > 0 {
		db = db.Where("id > ?", v)
	} else {
		offset := (paging.Page - 1) * paging.Limit
		if err := db.Offset(offset).Error; err != nil {
			return nil, common.ErrDB(err)
		}
	}

	if err := db.Limit(paging.Limit).Find(&amenities).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	return amenities, nil
}
