package placerepository

import (
	"context"

	placemodel "quickstart/internal/place/model"
	"quickstart/pkg/common"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type placerepository struct {
	db *gorm.DB
}

//constructor

func NewPlaceRepository(db *gorm.DB) *placerepository {
	return &placerepository{db}
}

// create place
func (repo *placerepository) Create(ctx context.Context, place *placemodel.Place) error {
	//apply transaction technique
	db := repo.db.Begin()
	if err := repo.db.Table(place.TableName()).Create(place).Error; err != nil {
		db.Rollback()
		return common.ErrDB(err)
	}
	if err := db.Commit().Error; err != nil {
		db.Rollback()
		return common.ErrDB(err)
	}

	return nil
}

// get place
func (repo *placerepository) ListDataWithCondition(ctx context.Context,
	paging *common.Paging,
	filter *placemodel.Filter,
	keys ...string) ([]placemodel.Place, error) {

	var data []placemodel.Place
	// db := repo.db.Table(placemodel.Place{}.TableName())
	//Để không count những record bị  soft delete ta cần dùng Model
	db := repo.db.Model(&data)

	if v := filter.OwnerId; v > 0 {
		db = db.Where("owner_id = ?", v)
	}
	if v := filter.CityId; v > 0 {
		db = db.Where("city_id = ?", v)
	}

	if err := db.Count(&paging.Total).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	for _, v := range keys {
		db = db.Preload(v)
	}

	if v := paging.Cursor; v > 0 {
		db = db.Where("id > ?", v)
	} else {
		offset := (paging.Page - 1) * paging.Limit
		if err := db.Offset(offset).Error; err != nil {
			return nil, common.ErrDB(err)
		}
	}
	if err := db.Limit(paging.Limit).Find(&data).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	if len(data) > 0 {
		paging.NextCursor = data[len(data)-1].Id
	}
	return data, nil
}

func (repo *placerepository) FindDataWithCondition(ctx context.Context, condition map[string]any, keys ...string) (*placemodel.Place, error) {
	var data placemodel.Place
	db := repo.db.Table(placemodel.Place{}.TableName())
	for _, v := range keys {
		db.Preload(v)
	}

	if err := db.Where(condition).First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.ErrEntityNotFound(placemodel.EntityName, err)
		}
		return nil, common.ErrDB(err)
	}
	return &data, nil
}

// Delete place by condition
func (repo *placerepository) DeleteDataWithCondition(ctx context.Context, condition map[string]any) error {
	if err := repo.db.Table(placemodel.Place{}.TableName()).Where(condition).Delete(&placemodel.Place{}).Error; err != nil {
		return common.ErrDB(err)
	}
	return nil
}

// update place by condition
func (repo *placerepository) UpdateDataWithCondition(ctx context.Context, condition map[string]any, place *placemodel.Place) error {

	if err := repo.db.Table(placemodel.Place{}.TableName()).Clauses(clause.Returning{}).Where(condition).Updates(place).Error; err != nil {
		return common.ErrDB(err)
	}
	return nil
}
