package placehttp

import (
	"context"
	"net/http"
	placemodel "quickstart/internal/place/model"
	"quickstart/pkg/common"
	"quickstart/pkg/utils"

	"github.com/gin-gonic/gin"
)

type PlaceUseCase interface {
	CreatePlace(ctx context.Context, place *placemodel.Place) error
	GetPlaces(ctx context.Context, paging *common.Paging, filter *placemodel.Filter) ([]placemodel.Place, error)
	GetPlaceById(ctx context.Context, id int) (*placemodel.Place, error)
	UpdatePlace(ctx context.Context, requester common.Requester, id int, place *placemodel.Place) error
	DeletePlace(ctx context.Context, requester common.Requester, id int) error
}

type placeHandler struct {
	placeUC PlaceUseCase
	hasher  *utils.Hasher
}

func NewPlaceHandler(placeUC PlaceUseCase, hasher *utils.Hasher) *placeHandler {
	return &placeHandler{placeUC, hasher}
}

func (handler *placeHandler) CreatePlace() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		requester := ctx.MustGet("user").(common.Requester)
		var place placemodel.Place
		//error occurs from binding json data into struct data
		if err := ctx.ShouldBind(&place); err != nil {
			panic(common.ErrBadRequest(err))
		}
		place.OwnerId = requester.GetUserId()

		// check error from usecase layer
		if err := handler.placeUC.CreatePlace(ctx.Request.Context(), &place); err != nil {
			panic(err)
		}
		//Encode id trước trả ra cho client
		place.FakeId = handler.hasher.Encode(place.Id, common.DBTypePlace)
		ctx.JSON(http.StatusOK, common.Response(place))
	}
}

func (handler *placeHandler) GetPlaces() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		//paging
		var paging common.Paging
		//error occurs from binding json data into struct data
		if err := ctx.ShouldBind(&paging); err != nil {
			panic(common.ErrBadRequest(err))
		}
		paging.Fulfill()
		//filter
		var filter placemodel.Filter
		if err := ctx.ShouldBind(&filter); err != nil {
			panic(common.ErrBadRequest(err))
		}
		// check error from usecase layer
		places, err := handler.placeUC.GetPlaces(ctx.Request.Context(), &paging, &filter)
		if err != nil {
			panic(err)
		}

		for i, v := range places {

			places[i].FakeId = handler.hasher.Encode(v.Id, common.DBTypePlace)
			// places[i].Owner.FakeId = handler.hasher.Encode(v.OwnerId, common.DBTypeUser)
		}

		ctx.JSON(http.StatusOK, common.ResponseWithPaging(places, paging))
	}
}

func (handler *placeHandler) GetPlaceByID() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// id, err := strconv.Atoi(ctx.Param("id"))
		id, err := handler.hasher.Decode(ctx.Param("id"))

		if err != nil {
			panic(common.ErrBadRequest(err))
		}

		place, err := handler.placeUC.GetPlaceById(ctx.Request.Context(), id)
		if err != nil {
			panic(err)
		}

		place.FakeId = handler.hasher.Encode(place.Id, common.DBTypePlace)
		place.Owner.FakeId = handler.hasher.Encode(place.OwnerId, common.DBTypeUser)
		ctx.JSON(http.StatusOK, common.Response(place))
	}
}

func (handler *placeHandler) UpdatePlace() gin.HandlerFunc {
	return func(ctx *gin.Context) {

		// lấy thông tin Requester
		requester := ctx.MustGet("user").(common.Requester)

		// id, err := strconv.Atoi(ctx.Param("id"))
		id, err := handler.hasher.Decode(ctx.Param("id"))
		if err != nil {
			panic(common.ErrBadRequest(err))
		}

		var place placemodel.Place
		if err := ctx.ShouldBind(&place); err != nil {
			panic(common.ErrBadRequest(err))
		}

		if err := handler.placeUC.UpdatePlace(ctx.Request.Context(), requester, id, &place); err != nil {
			panic(err)
		}

		place.FakeId = handler.hasher.Encode(place.Id, common.DBTypePlace)
		ctx.JSON(http.StatusOK, common.Response(place))
	}
}

func (handler *placeHandler) DeletePlace() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		requester := ctx.MustGet("user").(common.Requester)
		// id, err := strconv.Atoi(ctx.Param("id"))
		id, err := handler.hasher.Decode(ctx.Param("id"))
		if err != nil {
			panic(common.ErrBadRequest(err))
		}

		if err := handler.placeUC.DeletePlace(ctx.Request.Context(), requester, id); err != nil {
			panic(err)
		}

		ctx.JSON(http.StatusOK, common.Response(true))
	}
}
