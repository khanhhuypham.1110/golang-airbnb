package placeusecase

import (
	"context"
	"errors"
	"fmt"
	placemodel "quickstart/internal/place/model"
	"quickstart/pkg/common"
	"testing"
)

type mockPlaceRepository struct {
}

// Giả lập hàm create của place Repository
func (mockPlaceRepository) Create(ctx context.Context, place *placemodel.Place) error {
	if place.Name == "" {
		return common.ErrDB(errors.New("something went wrong in DB"))
	}
	place.Id = 101
	return nil
}

func (mockPlaceRepository) ListDataWithCondition(ctx context.Context, paging *common.Paging, filter *placemodel.Filter, key ...string) ([]placemodel.Place, error) {
	return nil, nil
}
func (mockPlaceRepository) FindDataWithCondition(ctx context.Context, condition map[string]any, key ...string) (*placemodel.Place, error) {
	return nil, nil
}
func (mockPlaceRepository) UpdateDataWithCondition(ctx context.Context, condition map[string]any, place *placemodel.Place) error {
	return nil
}
func (mockPlaceRepository) DeleteDataWithCondition(ctx context.Context, condition map[string]any) error {
	return nil
}

func TestCreatePlace(t *testing.T) {
	placeUC := NewPlaceUseCase(mockPlaceRepository{})
	//Test những trường hợp lỗi
	dataTable := []struct {
		input    placemodel.Place
		expected string
	}{
		{input: placemodel.Place{Name: "", Address: "Nguyễn Huệ, Q1"}, expected: placemodel.ErrNameIsEmpty.Error()},
		{input: placemodel.Place{Name: "Candy Home", Address: ""}, expected: placemodel.ErrAddressIsEmpty.Error()},
	}

	for _, item := range dataTable {
		err := placeUC.CreatePlace(context.Background(), &item.input)
		fmt.Println(err)
		if err.Error() != item.expected {
			t.Errorf("create place - Input %v, Expected: %v, Output: %v", item.input, item.expected, err)
		}
	}

	//Test trường hợp thành công, expect là không trả về lỗi
	dataTest := placemodel.Place{Name: "SweetHome", Address: "CMT8, Q3"}
	err := placeUC.CreatePlace(context.Background(), &dataTest)
	if err != nil {
		t.Errorf("create place - Input: %v, Output: %v", dataTest, err)
	}

}
