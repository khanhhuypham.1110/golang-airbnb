package placeusecase

import (
	"context"
	placemodel "quickstart/internal/place/model"
	"quickstart/pkg/common"
)

type PlaceRepository interface {
	Create(ctx context.Context, place *placemodel.Place) error
	ListDataWithCondition(ctx context.Context, paging *common.Paging, filter *placemodel.Filter, key ...string) ([]placemodel.Place, error)
	FindDataWithCondition(ctx context.Context, condition map[string]any, key ...string) (*placemodel.Place, error)
	UpdateDataWithCondition(ctx context.Context, condition map[string]any, place *placemodel.Place) error
	DeleteDataWithCondition(ctx context.Context, condition map[string]any) error
}

type placeUsecase struct {
	placeRepo PlaceRepository
}

func NewPlaceUseCase(placeRepo PlaceRepository) *placeUsecase {
	return &placeUsecase{placeRepo}
}

func (placeUC *placeUsecase) CreatePlace(ctx context.Context, place *placemodel.Place) error {
	//------perform business operation such as validate data
	if err := place.Validate(); err != nil {
		return common.ErrBadRequest(err)
	}
	//------
	if err := placeUC.placeRepo.Create(ctx, place); err != nil {
		return common.ErrCannotCreateEntity(placemodel.EntityName, err)
	}
	return nil
}

func (placeUC *placeUsecase) GetPlaces(ctx context.Context, paging *common.Paging, filter *placemodel.Filter) ([]placemodel.Place, error) {
	//there will have business logic before getting data list with condition
	places, err := placeUC.placeRepo.ListDataWithCondition(ctx, paging, filter, "Owner", "Location")

	if err != nil {
		return nil, common.ErrCannotListEntity(placemodel.EntityName, err)
	}
	return places, nil
}

func (placeUC *placeUsecase) GetPlaceById(ctx context.Context, id int) (*placemodel.Place, error) {
	//there will have business logic before getting specific data with condition

	place, err := placeUC.placeRepo.FindDataWithCondition(ctx, map[string]any{"id": id}, "Owner", "Location")
	if err != nil {
		return nil, common.ErrEntityNotFound(placemodel.EntityName, err)
	}
	return place, nil
}

func (placcUC *placeUsecase) UpdatePlace(ctx context.Context, requester common.Requester, id int, place *placemodel.Place) error {
	//validate the data first under this usecase layer
	if err := place.Validate(); err != nil {
		return err
	}
	//check the eixstence of data in database
	currentPlace, err := placcUC.placeRepo.FindDataWithCondition(ctx, map[string]any{"id": id})
	if err != nil {
		return common.ErrEntityNotFound(placemodel.EntityName, err)
	}

	// if there is no error occurs, we call the UpdatePlaceWithCondition of replaceRepo interface

	if requester.GetUserRole() != "admin" && currentPlace.OwnerId != requester.GetUserId() {
		return common.ErrForbidden(nil)
	}

	if err := placcUC.placeRepo.UpdateDataWithCondition(ctx, map[string]any{"id": id}, place); err != nil {
		return common.ErrCannotUpdateEntity(placemodel.EntityName, err)
	}
	return nil
}

func (placeUC *placeUsecase) DeletePlace(ctx context.Context, requester common.Requester, id int) error {
	//check the existence of data in database
	currentPlace, err := placeUC.placeRepo.FindDataWithCondition(ctx, map[string]any{"id": id})

	if err != nil {
		return common.ErrEntityNotFound(placemodel.EntityName, err)
	}

	if requester.GetUserRole() != "admin" && currentPlace.OwnerId != requester.GetUserId() {
		return common.ErrForbidden(nil)
	}

	//if there is no returned error, we call the method DeleteDataByCondition of placeRepo interface
	if err := placeUC.placeRepo.DeleteDataWithCondition(ctx, map[string]any{"id": id}); err != nil {
		return common.ErrCannotDeleteEntity(placemodel.EntityName, err)
	}
	return nil
}
