package placemodel

import (
	"errors"

	locationmodel "quickstart/internal/location/model"
	"quickstart/pkg/common"
	"strings"
)

const EntityName = "place"

type Place struct {
	common.SQLModel
	Name           string                  `json:"name" gorm:"column:name"`
	Description    string                  `json:"description" gorm:"column:description"`
	TotalGuests    int                     `json:"totalGuests" gorm:"column:total_guests"`
	TotalBedrooms  int                     `json:"totalBedrooms" gorm:"column:total_bedrooms"`
	TotalBathrooms int                     `json:"totalBathrooms" gorm:"column:total_bathrooms"`
	PricePerNight  float64                 `json:"pricePerNight" gorm:"column:price_per_night"`
	Average_rating float64                 `json:"averageRating" gorm:"column:average_rating"`
	OwnerId        int                     `json:"-" gorm:"column:owner_id"`
	Owner          *common.SimpleUser      `json:"owner" gorm:"preload:false"`
	LocationId     int                     `json:"locationId" gorm:"column:location_id"`
	Location       *locationmodel.Location `json:"location" gorm:"preload:false"`
	Address        string                  `json:"address" gorm:"column:address"`
	Cover          *common.Images          `json:"cover" gorm:"column:cover"`
	Lat            float64                 `json:"lat" gorm:"column:latitude"`
	Lng            float64                 `json:"lng" gorm:"column:longtitude"`
}

func (Place) TableName() string {
	return "places"
}

func (p *Place) Validate() error {
	p.Name = strings.TrimSpace(p.Name)
	if p.Name == "" {
		return ErrNameIsEmpty
	}

	p.Address = strings.TrimSpace(p.Address)
	if p.Address == "" {
		return ErrAddressIsEmpty
	}

	return nil
}

var (
	ErrNameIsEmpty    = common.NewCustomErr(errors.New("place name can't be blank"), "place name can't be blank")
	ErrAddressIsEmpty = common.NewCustomErr(errors.New("place address can't be blank"), "place address can't be blank")
)
