package userrepository

import (
	"context"
	"fmt"
	usermodel "quickstart/internal/user/model"
	"quickstart/pkg/common"

	"gorm.io/gorm"
)

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *userRepository {
	return &userRepository{db}
}

func (userRepo *userRepository) Create(ctx context.Context, userCreate *usermodel.UserCreate) error {
	db := userRepo.db.Begin()
	fmt.Println("usercreate", userCreate)
	if err := db.Table(usermodel.User{}.TableName()).Create(userCreate).Error; err != nil {
		db.Rollback()
		fmt.Println("err", err)
		return common.ErrDB(err)
	}
	if err := db.Commit().Error; err != nil {
		db.Rollback()
		return common.ErrDB(err)
	}
	return nil
}

func (userRepo *userRepository) FindDataWithCondition(ctx context.Context, condition map[string]any) (*usermodel.User, error) {
	var user usermodel.User
	if err := userRepo.db.Table(usermodel.User{}.TableName()).Where(condition).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.ErrEntityNotFound(usermodel.EntityName, err)
		}
		return nil, common.ErrDB(err)
	}
	return &user, nil
}

func (userRepo *userRepository) DeleteUserWithCondition(ctx context.Context, condition map[string]any) error {

	if err := userRepo.db.Table(usermodel.User{}.TableName()).Where(condition).Delete(&usermodel.User{}).Error; err != nil {
		return common.ErrDB(err)
	}
	return nil
}
