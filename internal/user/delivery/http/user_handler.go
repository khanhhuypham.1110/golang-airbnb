package userhttp

import (
	"context"
	"net/http"
	usermodel "quickstart/internal/user/model"
	"quickstart/pkg/common"
	"quickstart/pkg/utils"

	"github.com/gin-gonic/gin"
)

type UserUserCase interface {
	Register(ctx context.Context, userCreate *usermodel.UserCreate) error
	Login(ctx context.Context, credentials *usermodel.UserLogin) (*utils.Token, error)
	DeleteUserById(ctx context.Context, id int) error
}

type userHandler struct {
	userUC UserUserCase
	hasher *utils.Hasher
}

func NewUserHandler(userUC UserUserCase, hasher *utils.Hasher) *userHandler {
	return &userHandler{userUC: userUC, hasher: hasher}
}

func (handler *userHandler) Register() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var userCreate usermodel.UserCreate
		//get data Input from client
		if err := ctx.ShouldBind(&userCreate); err != nil {
			panic(common.ErrBadRequest(err))
		}
		if err := handler.userUC.Register(ctx.Request.Context(), &userCreate); err != nil {
			panic(err)
		}
		ctx.JSON(http.StatusCreated, gin.H{
			"userId": userCreate.Id,
		})
	}
}

func (handler *userHandler) Login() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var credentials usermodel.UserLogin
		if err := ctx.ShouldBind(&credentials); err != nil {
			panic(common.ErrBadRequest(err))
		}
		token, err := handler.userUC.Login(ctx.Request.Context(), &credentials)
		if err != nil {
			panic(common.ErrBadRequest(err))
		}
		ctx.JSON(http.StatusOK, gin.H{
			"data": token,
		})
	}
}

func (handler *userHandler) DeleteUserById() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := handler.hasher.Decode(ctx.Param("id"))
		if err != nil {
			panic(common.ErrBadRequest(err))
		}

		if err := handler.userUC.DeleteUserById(ctx, id); err != nil {
			panic(err)
		}

		ctx.JSON(http.StatusOK, gin.H{"delete successfully": ctx.Param("id")})
	}
}
