package usermodel

import (
	"errors"
	"quickstart/pkg/common"
)

var (
	ErrEmailOrPasswordInvalid = common.NewCustomErr(errors.New("email or password invalid"), "emai or password invalid")
	ErrEmailExisted           = common.NewCustomErr(errors.New("email has already existed"), "email has already existed")
	ErrorCannotCreateAccount  = common.NewCustomErr(errors.New("can not create your account"), "can not create your account")
)
