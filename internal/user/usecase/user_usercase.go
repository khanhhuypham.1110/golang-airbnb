package userusecase

import (
	"context"
	"quickstart/config"
	usermodel "quickstart/internal/user/model"
	"quickstart/pkg/common"
	"quickstart/pkg/utils"
)

type UserRepository interface {
	Create(ctx context.Context, userCreate *usermodel.UserCreate) error
	FindDataWithCondition(ctx context.Context, condition map[string]any) (*usermodel.User, error)
	DeleteUserWithCondition(ctx context.Context, condition map[string]any) error
}

type userUserCase struct {
	cfg      *config.Config
	userRepo UserRepository
}

func NewUserUseCase(userRepository UserRepository, cfg *config.Config) *userUserCase {
	return &userUserCase{userRepo: userRepository, cfg: cfg}
}

func (userUc *userUserCase) Register(ctx context.Context, data *usermodel.UserCreate) error {
	user, _ := userUc.userRepo.FindDataWithCondition(ctx, map[string]any{"email": data.Email})
	if user != nil {
		return usermodel.ErrEmailExisted
	}
	/*process data input such as hashing pwd and validate*/
	//validate data
	if err := data.Validate(); err != nil {
		return err
	}
	if err := data.PrepareCreate(); err != nil {
		return usermodel.ErrEmailOrPasswordInvalid
	}
	if err := userUc.userRepo.Create(ctx, data); err != nil {
		return usermodel.ErrorCannotCreateAccount
	}
	return nil
}

func (userUC *userUserCase) Login(ctx context.Context, data *usermodel.UserLogin) (*utils.Token, error) {
	user, err := userUC.userRepo.FindDataWithCondition(ctx, map[string]any{"email": data.Email})
	if err != nil {
		return nil, usermodel.ErrEmailOrPasswordInvalid
	}
	if err := utils.CheckPasswordHash(user.Password, data.Password); err != nil {
		return nil, usermodel.ErrEmailOrPasswordInvalid
	}
	token, err := utils.GenerateJWT(utils.TokenPayload{Email: user.Email, Role: user.Role}, userUC.cfg)
	if err != nil {
		return nil, common.ErrInternal(err)
	}
	return token, nil
}

func (userUC userUserCase) DeleteUserById(ctx context.Context, id int) error {
	_, err := userUC.userRepo.FindDataWithCondition(ctx, map[string]any{"id": id})
	if err != nil {
		return common.ErrEntityNotFound(usermodel.EntityName, err)
	}

	if err := userUC.userRepo.DeleteUserWithCondition(ctx, map[string]any{"id": id}); err != nil {
		return common.ErrCannotDeleteEntity(usermodel.EntityName, err)
	}
	return nil
}
