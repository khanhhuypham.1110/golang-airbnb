package middleware

import (
	"fmt"
	"quickstart/pkg/common"

	"github.com/gin-gonic/gin"
)

func (m *middlewareManager) Recover() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			err := recover()
			fmt.Println(err)
			if err != nil {
				c.Header("Content-Type", "application/json")
				if appErr, ok := err.(*common.AppError); ok {
					c.AbortWithStatusJSON(appErr.StatusCode, appErr)
					//để có thể chạy tới middleware Recover mặc định của Gin ta cần tiếp tục panic err
					// panic(err)
					return
				}
				appErr := common.ErrInternal(err.(error))
				c.AbortWithStatusJSON(appErr.StatusCode, appErr)
				// panic(err)
				return
			}
		}()
		c.Next()
	}
}
