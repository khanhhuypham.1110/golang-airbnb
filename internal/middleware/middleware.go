package middleware

import (
	"context"
	"quickstart/config"
	usermodel "quickstart/internal/user/model"
)

type UserRepository interface {
	FindDataWithCondition(context.Context, map[string]any) (*usermodel.User, error)
}

type middlewareManager struct {
	configuration *config.Config
	userRepo      UserRepository
}

func NewMiddlewareManager(cfg *config.Config, userRepo UserRepository) *middlewareManager {
	return &middlewareManager{configuration: cfg, userRepo: userRepo}
}
