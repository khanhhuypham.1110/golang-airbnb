package middleware

import (
	usermodel "quickstart/internal/user/model"
	"quickstart/pkg/common"

	"github.com/gin-gonic/gin"
)

//Cần phải gọi middlewares RequireAuth trước

func (m *middlewareManager) RequiredRoles(roles ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
		user := c.MustGet("user").(*usermodel.User)

		for i := range roles {
			if user.GetUserRole() == roles[i] {
				c.Next()
				return
			}
		}
		panic(common.ErrForbidden(nil))

	}
}
