package bookingmodel

import (
	"quickstart/pkg/common"
	"time"
)

type Booking struct {
	common.SQLModel
	UserId       int
	PlaceId      int
	CheckingDate time.Time
	CheckoutDate time.Time
	Discount     float64
	TotalPrice   float64
}
