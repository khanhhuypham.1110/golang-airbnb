package config

import (
	"errors"

	"github.com/spf13/viper"
)

type Config struct {
	App   AppConfig
	MySQL MySQLConfig
	AWS   AWSConfig
	Redis RedisConfig
}

type AppConfig struct {
	Version      string
	Mode         string
	Port         string
	Secret       string
	MigrationURL string
}

type MySQLConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	DBName   string
}

type AWSConfig struct {
	Region    string
	APIKey    string
	SecretKey string
	S3Domain  string
	S3Bucket  string
}

type RedisConfig struct {
	Host     string
	Port     string
	Password string
	DB       int
}

func LoadConfig(fileName string) (*Config, error) {
	v := viper.New()
	v.SetConfigFile(fileName)
	var config Config
	if err := v.ReadInConfig(); err != nil {
		// check the existence of the config file, if not found return err immediately
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			return nil, errors.New("unable to find config file")
		}
		return nil, err
	}

	//when config file can be read, we bind data from 'yml' file to the pointer variable of config
	if err := v.Unmarshal(&config); err != nil {
		return nil, err
	}

	return &config, nil

}
